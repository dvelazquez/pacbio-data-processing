ZMWs input               : 12

ZMWs pass filters        : 12 (100.00%)
ZMWs fail filters        : 0 (0.00%)
ZMWs shortcut filters    : 0 (0.00%)

ZMWs with tandem repeats : 0 (0.00%)

Exclusive counts for ZMWs failing filters:
Below SNR threshold      : 0 (0.00%)
Median length filter     : 0 (0.00%)
Lacking full passes      : 0 (0.00%)
Heteroduplex insertions  : 0 (0.00%)
Coverage drops           : 0 (0.00%)
Insufficient draft cov   : 0 (0.00%)
Draft too different      : 0 (0.00%)
Draft generation error   : 0 (0.00%)
Draft above --max-length : 0 (0.00%)
Draft below --min-length : 0 (0.00%)
Reads failed polishing   : 0 (0.00%)
Empty coverage windows   : 0 (0.00%)
CCS did not converge     : 0 (0.00%)
CCS below minimum RQ     : 0 (0.00%)
Unknown error            : 0 (0.00%)

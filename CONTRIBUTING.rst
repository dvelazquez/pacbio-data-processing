.. _contributing:

How to contribute
=================

Thank you for considering it!

If you found a bug, have ideas about new functionality or suggestions to
improve |project|, including its documentation, please, visit our
repository hosted in Gitlab (|repo_url|) and have a look at the issues.

If you want to contribute with code, please, have a look at our
:ref:`development-notes`. Hopefully they help you understand how we
develop |project|.
But feel free to ask or to create an issue in our Gitlab page |repo_url|
if you feel confused about how to start contributing.

.. highlight:: python

******************
Coding conventions
******************

* Follow PEP8
* Use ``Flake8`` as linter


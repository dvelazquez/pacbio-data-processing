#########################
Developer's documentation
#########################

This document describes some guidelines for developers.

.. toctree::
   :maxdepth: 2

   development
   notes
   release
   requirements
   roadmap
   style
   design
   modules
   ../contributing
   ../authors
   ../history

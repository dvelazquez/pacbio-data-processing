.. highlight:: python

.. _as-library:

Using the package as a library
==============================

|project| has not been thought to be used as a library. The project's aim
is to provide an executable  program, :ref:`sm-analysis <sm-analysis>` (and another utility
kept for historical reasons :ref:`bam-filter <bam-filter>`).

Nevertheless, if you think that some functionality in |project| could help
in your own codebase, feel free to::

    import pacbio_data_processing

or whatever other module you are interested in.

And, please, have a look at :ref:`contributing`. Maybe you have an idea to
improve the project. We will be glad to hear about that.

.. _usage:

=====
Usage
=====

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   bam_filter
   sm_analysis
   as_library


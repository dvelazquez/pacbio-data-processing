
.. _guides:

======
Guides
======

This section contains more in-depth documents describing
|project|, its operation and outputs as well as other
related topics.

.. toctree::
   :maxdepth: 2

   sm-analysisdoc
   bam
   summary_reports
   methylation_reports
   raw_detections
   phred_transformed_scores
   cluster
   pacbio_tools
   pbindex
   aligners
   ccs
   kinetics_tools
   htslib
   spack
   python
   bioconda
   pacbio_chemistries
   references
   glossary
